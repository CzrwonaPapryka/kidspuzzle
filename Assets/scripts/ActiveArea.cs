﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveArea : MonoBehaviour {


	public List<GameObject> appear = new List<GameObject>();
	public List<GameObject> disappear = new List<GameObject>();
	public bool destroy;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {


		if (Input.GetMouseButton (0) ) 
		{
			
		}


	}

	void OnMouseDown()
	{

		//Debug.Log (disappear.Count);
		//Debug.Log (appear.Count);

		for (int i = 0; i < disappear.Count; i++) 
		{
			if (disappear[i]) 
			{
				disappear [i].SetActive (false);
				//definition to disappear the object
				//Debug.Log (disappear[i]);
			}

		}
		for (int i = 0; i < appear.Count; i++) 
		{
			if (appear[i]) 
			{
				appear [i].SetActive (true);
				//definition to appear the object
				//Debug.Log (appear[i]);
			}

		}


		if (destroy) 
		{
			Destroy (this.gameObject);
		}
	}

}
