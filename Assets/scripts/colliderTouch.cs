﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colliderTouch : MonoBehaviour {

	public GameObject gObj;
	public List<GameObject> appear = new List<GameObject>();
	public List<GameObject> disappear = new List<GameObject>();


	void OnTriggerEnter(Collider col)
	{
		

		if(col.gameObject.name == gObj.gameObject.name)
		{
			Debug.Log ("hit");
			SoundManager.PlaySound ("3");//dopasowanie
			for (int i = 0; i < disappear.Count; i++) 
			{
				if (disappear[i]) 
				{
					disappear [i].SetActive (false);

				}

			}
			for (int i = 0; i < appear.Count; i++) 
			{
				if (appear[i]) 
				{
					appear [i].SetActive (true);

				}

			}

		}
	}
		

}
