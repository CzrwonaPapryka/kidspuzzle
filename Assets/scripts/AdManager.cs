﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using admob;

public class AdManager : MonoBehaviour {


	public static AdManager Instance{ set; get;}

	public string BanerId;
	public string VideoId;

	private void Start()
	{
		Instance = this;
		DontDestroyOnLoad (gameObject);
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		Admob.Instance ().initAdmob (BanerId, VideoId);
		//Admob.Instance().setTesting(true); - odkomentować jeżeli będziemy chcieli testować
		Admob.Instance ().loadInterstitial ();
		#endif
	}

	public void ShowBanner ()
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		Admob.Instance ().showBannerRelative (AdSize.Banner, AdPosition.TOP_CENTER, 5);
		#endif
	}

	public void ShowVideo ()
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		if (Admob.Instance ().isInterstitialReady ()) 
		{
			Admob.Instance ().showInterstitial ();
		}
		#endif
	}


	// umieszczasz gdziekolwiek w grze gdzie chcesz żeby ię wyświetliło 
	//AdManager.Instance.ShowBanner();  - żeby wyświelić baner
	//AdManager.Instance.ShowVideo();  - żeby wyświelić Video




}
