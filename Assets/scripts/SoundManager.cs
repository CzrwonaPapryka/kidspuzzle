﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {


	public static AudioClip playerClickSound;
	public static AudioClip playerClickSound2;
	public static AudioClip playerClickSound3;
	public static AudioClip playerClickSound4;
	static AudioSource audioscr;
	// Use this for initialization
	void Start () {
		playerClickSound = Resources.Load<AudioClip> ("click_01");
		playerClickSound2 = Resources.Load<AudioClip> ("click_02");
		playerClickSound3 = Resources.Load<AudioClip> ("buy_02");
		playerClickSound4 = Resources.Load<AudioClip> ("time_01");

		audioscr = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public static void PlaySound(string clip)
	{
		switch (clip) 
		{
		case "1":
			audioscr.PlayOneShot (playerClickSound);
			break;

		case "2":
			audioscr.PlayOneShot (playerClickSound2);
			break;
		case "3":
			audioscr.PlayOneShot (playerClickSound3);
			break;
		case "4":
			audioscr.PlayOneShot (playerClickSound4);
			break;
		}

	}


}
